# Bots - Remote Administration Tool


## Getting Started
Download the repository using the following commands:
```bash
wget https://gitlab.com/murster972/Bots/-/archive/master/Bots-master.zip
unzip Bots-master.zip
```

### Prerequisites and Install
Run install.py, it will download/install the nessecary packages and python3 modules (listed below) as-well
as creating a config file (config.ini).

**Packages:**
- python3-pip - used for install
- speedtest-cli - used to get network speeds
- net-tools - ip command
- mysql-server - database
- gunicorn - python htttp server

**Modules:**
- python3-mysqldb - module used to interface with database
- flask - webframework
- psutil - used to get device resource info

### Running
**UI**

**BotMaster**

run the file bot_master.py with sudo, it will use the IP and Port specififed in config.ini, e.g.
```bash
(Bots) sudo python3 bot_master.py
```

**Bot**

Once BotMaster is running bots can connected to it. Run bots.py as you would any python3 program and when
prompted enter the IP address and Port number used by BotMaster, e.g.
```bash
(Bots) python3 bot.py
(Bots) BotMaster IP: 127.0.0.1
(Bots) BotMaster Port: 12
```

## Author/s
- Murray Watson : https://gitlab.com/murster972

## Acknowledgement/s
- Abby Gourlay, helped with testing and debugging : https://gitlab.com/TrueTechy

## License
See file (LICENSE) in master branch of repository

## Future features
- [ ] peripheral devices - monitor and control
    - Keyboard, mouse, monitor, USB, etc.
- [ ] Vulnerability scanning/exploitation
    - Bots scan other devices on their network for vulnerabilities
    - exploit said vulnerabilities and connect device to BotMaster
- [ ] File Transfer
    - Download/upload files to and from connected Bots
