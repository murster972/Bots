/* creates and sets current DB to Bots */
CREATE DATABASE Bots_DB;
USE Bots_DB;

/* creates tables used in Bots DB */
CREATE TABLE Bots(
    MAC_Addr VARCHAR(22) NOT NULL,
    IP_Addr VARCHAR(15) NOT NULL,
    port_no INT NOT NULL,
    connected_time DATETIME,
    name VARCHAR(255),
    status INT,
    OS_name VARCHAR(255),
    cpu_model VARCHAR(255),
    cpu_perct FLOAT,
    cpu_core_percts VARCHAR(255),
    ram_total FLOAT,
    ram_used FLOAT,
    swap_total FLOAT,
    swap_used FLOAT,
    dl_speed FLOAT,
    ul_speed FLOAT,
    s_number INT,
    s_size FLOAT,
    r_number INT,
    r_size FLOAT,
    storage_devices TEXT,
    PRIMARY KEY (MAC_Addr),
    CONSTRAINT MAC_CHECK CHECK (LENGTH(MAC_Addr)=22),
    CONSTRAINT IP_CHECK CHECK (LENGTH(IP_Addr)=15)
);

CREATE TABLE Commands(
    ID INT NOT NULL AUTO_INCREMENT,
    crt_time DATETIME NOT NULL,
    cmd TEXT NOT NULL,
    PRIMARY KEY (ID)
);

CREATE TABLE Bot_Commands(
    cmdID INT NOT NULL,
    botID VARCHAR(22) NOT NULL,
    Output TEXT,
    srt_time FLOAT,
    run_time FLOAT,
    Sent BOOL DEFAULT 0 NOT NULL,
    PRIMARY KEY (cmdID, botID),
    FOREIGN KEY (cmdID) REFERENCES Commands(ID),
    FOREIGN KEY (botID) REFERENCES Bots(MAC_Addr)
);

CREATE TABLE BotMaster(
    ID INT NOT NULL,
    IP_Addr VARCHAR(15) NOT NULL,
    port INT NOT NULL,
    ram_perct FLOAT NOT NULL,
    cpu_perct FLOAT NOT NULL,
    PRIMARY KEY (ID)
);

/* adds entry for botMaster */
INSERT INTO BotMaster
VALUES (1, "", 0, 0, 0);
