#!/usr/bin/env python3

TYPE = {"info": "\u001b[46m", "error": "\u001b[41;1m"}
MSG = {"info": "\u001b[36m", "error": "\u001b[31;1m"}
DATA  = {"info": "\u001b[37;1m", "error": "\u001b[37;1m"}

RS = "\u001b[0m"

def colourp(type, msg, data=""):
    c1, c2, c3 = TYPE[type], MSG[type], DATA[type]
    t = type.upper()

    # removes trailing : if no data is passed
    data_ = ": "  + data if data else ""

    print("{} {} {}{} {}{}{}{}{}".format(c1, t, RS, c2, msg,  RS, c3, data_, RS))

if __name__ == '__main__':
    colourp("error", "BotMaster is unavaiable", "SHUTTING DOWN")
