#!/usr/bin/env python3
import sys

''' checks if an ip addr is valid '''
def verify_ip(ip):
    if ip == "": return 0

    try:
        sects = list(map(int, ip.split(".")))
        sects = [1 if i >= 0 and i <= 255 else 0 for i in sects]

        return 1 if sum(sects) != 4 else 0

    except ValueError:
        return 1

''' checks if a port no is valid '''
def verify_port(port):
    try:
        p = int(port)

        return 1 if p < 1 or p > 65535 else 0

    except ValueError:
        return 1

def validate_ip_port(ip, port):
    #verify ip and port
    vip = verify_ip(ip)
    vport = verify_port(port)

    if vip and vport:
        print("[ERROR] Invalid format for IP and port number not in valid range.")
    elif vip:
        print("[ERROR] IP entered is not the correct format.")
    elif vport:
        print("[ERROR] Port number not in valid range.")
    if vip or vport: sys.exit(-1)
