#!/usr/bin/env python3
import subprocess

''' runs speedtest cmd and returns output '''
def run_cmd():
    res = subprocess.run(["speedtest", "--simple"], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    return res.stdout.decode()

''' parses output of speedtest cmd to get dl and ul speeds '''
def speed_test():
    speeds = run_cmd().split("\n")

    #checks if unable to run speed test and retuns zero for ul/dl if
    #test could not be ran
    if not speeds[0]:
        return (0, 0)

    #test ran successfully
    #extracts dl/ul from output
    dl = float(speeds[1].split(":")[1].split()[0])
    ul = float(speeds[2].split(":")[1].split()[0])

    return (dl, ul)

if __name__ == '__main__':
    s = speed_test()
    print(s)
