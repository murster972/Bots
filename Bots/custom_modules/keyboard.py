#!/usr/bin/env python3
import subprocess
import struct
import os

class KeyboardReader:
    INPUT_DIR = "/dev/input/"

    def __init__(self):
        event_file = self.__get_event_file()

        print(event_file)

        if event_file == -1:
            raise KeyboardEventFileError("unable to find event file for keyboard.")

        self.kb_event_file = KeyboardReader.INPUT_DIR + event_file

        self.event_codes = self.__parse_event_codes()

        self.read_keyboard_input()

    def read_keyboard_input(self):
        ''' reads keyboard event file and parses output to get user keystrokes '''
        # structure (data-types) used in event file
        # https://www.kernel.org/doc/Documentation/input/input.txt
        # ^section 5
        # struct input_event {
        # 	struct timeval time {
        #       long tv_sec;
        #       long tv_usec;
        #   };
        # 	unsigned short type;
        # 	unsigned short code;
        # 	unsigned int value;
        # };

        # data stuct of each key stroke entry - explained above
        # l = long, H = unsigned short, I = unisgned int
        data_struct = "llHHI"
        data_struct_size = struct.calcsize(data_struct)

        print(self.event_codes)

        # read strokes from kb event file
        with open(self.kb_event_file, "rb") as f:
            while True:
                data = f.read(data_struct_size)
                tv_sec, tv_usec, type, code, value = struct.unpack(data_struct, data)

                print(type, code, value)
                print(self.event_codes[code])

    def __get_event_file(self):
        ''' finds the the file used by keyboard in /dev/input/ '''
        # get output of /proc/bus/input/devices file
        output = self._run_command("cat /proc/bus/input/devices")

        # parses file to get all device sections
        devices = list(map(lambda x: x.split("\n"), output.split("\n\n")))

        # finds keyboard devices and gets their handler info
        for device in devices:
            if not "keyboard" in "".join(device).lower():
                continue

            # gets handler info for keyboard
            handler = [i for i in device if i[0] == "H"][0].split(": ")
            handler_files = handler[1].split("=")[1].split()

            # gets event file from handler info
            event_file = [i for i in handler_files if "event" in i]

            # returns event file if the keyboard has one as a handler
            if event_file: return event_file[0]

        # return indicates no event file was found
        return -1

    def __parse_event_codes(self):
        ''' parses C header file containing mapping of event codes to ascii values
            file: input-event-codes.h '''
        parsed = {}

        with open("input-event-codes.h", "r") as f:
            for line in f.readlines():
                # skip lines that don't contain a #DEFINE
                if line[:7] != "#define": continue

                # split line into [#DEFINE, DESCR, CODE, *comments]
                # replace tab with space to allow easier splitting
                tmp = line.replace("\t", " ").split()

                # splits descr to get firt part, i.e. KEY, BUTN, etc.
                descr = tmp[1].split("_")
                descr_type = descr[0]
                descr_val = descr[1]

                # skips line if it does not contain a code value or
                # if it is not a KEY value
                # or if CODE is not an int value
                if len(tmp) < 3 or descr_type != "KEY":
                    continue

                try:
                    code = int(tmp[2])
                except ValueError:
                    # skips line if code is not an int value
                    continue

                parsed[descr_val] = code

        # reverses so values are keys
        return {parsed[key]:key for key in parsed}

    ''' runs command and returns output '''
    def _run_command(self, cmd):
        res = subprocess.run(cmd.split(), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        return (res.stdout if res.stdout else res.stderr).decode()

class KeyboardEventFileError(Exception):
    pass

if __name__ == '__main__':
    KeyboardReader()
