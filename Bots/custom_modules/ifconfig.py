#!/usr/bin/env python3
import subprocess

# NOTE: OBSOLETE - use itnerface_info.py instead

def run_command():
    res = subprocess.run("ifconfig", stdout=subprocess.PIPE)
    return res.stdout.decode()

''' parses ifconfig output and return as python dict '''
def parse_ifconfig():
    #get output of ifconfig
    output = run_command()

    interfaces = {}

    #split into interface sections
    if_sects = output.split("\n\n")[:-1]

    #goes through each interface getting info
    for intf in if_sects:
        lines = intf.split("\n")

        if_name = lines[0].split(":")[0]

        interfaces[if_name] = {}

        #gets mac addr if one
        if "ether" in intf:
            #extracts ether info
            l = [i.strip().split() for i in lines if "ether" in i.split()]

            #checks l has correct outut and gets mac-addr
            if len(l) >= 1: interfaces[if_name]["ether"] = l[0][1]

        #gets ipv4 addr and mask
        if "inet" in intf:
            l = [i.strip().split() for i in lines if "inet" in i.split()]
            try:
                interfaces[if_name]["ip"] = l[0][1]
                interfaces[if_name]["mask"] = l[0][3]
            except IndexError:
                #unable to get ip or mask from ifconfig
                interfaces[if_name]["ip"] = ""
                interfaces[if_name]["mask"] = ""

        #gets sent/recv packet stats
        for i in "sent recv".split():
            filter = ("TX" if i == "sent" else "RX") + " packets"
            l = [i.strip().split() for i in lines if filter in i]

            key = i + "_packets"
            interfaces[if_name][key] = {}
            interfaces[if_name][key]["number"] = 0 if not l else int(l[0][2])
            interfaces[if_name][key]["size"] = 0 if not l else int(l[0][4])

    return interfaces

if __name__ == '__main__':
    x = parse_ifconfig()
    print(x)
