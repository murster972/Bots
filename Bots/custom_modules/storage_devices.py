#!/usr/bin/env python3
import subprocess

def get_storage_devices():
    ''' returns info of all storage devices and their partitions '''
    # run fdisk and get output of attached devices and their partitions
    cmd = "df -h".split()
    res = subprocess.run(cmd, stdout=subprocess.PIPE)
    res = res.stdout.decode().split("\n")

    storage = []

    # parse results of fdisk
    for ind in range(1, len(res) - 1):
        tmp = res[ind].split()

        device = {"filesytem": tmp[0], "size": tmp[1], "used": tmp[2],
                  "avail": tmp[3], "perct": tmp[4], "mounted": tmp[5]}
        storage.append(device)

    return storage

if __name__ == '__main__':
    get_storage_devices()
