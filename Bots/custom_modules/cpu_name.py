#!/usr/bin/env python3
import sys
import platform
import subprocess

''' runs command to get cpu linux info and returns output '''
def cpu_linux():
    res = subprocess.run(["cat", "/proc/cpuinfo"], stdout=subprocess.PIPE)
    return res.stdout.decode().split("\n")

''' return the name of cpu used '''
def get_cpu_name():
    os = sys.platform

    # checks if linux or windows and gets cpu using approp method
    if os == "win32":
        return platform.processor()
    elif os == "linux" or os == "linux2":
        cmd = cpu_linux()

        # parses cmd for cpu name
        for i in cmd:
            if "model name" not in i: continue

            # extracts and returns name from line
            line = i.split(":")

            return line[1].strip()

    else:
        raise Exception("Unable to get CPU info for non Windows or Linux machines.")
