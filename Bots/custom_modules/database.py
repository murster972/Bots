import MySQLdb
from configparser import ConfigParser

import os

def connect_to_db():
    ''' Establishes connection to Bots database
        :output: database connection object if successfull else 0 '''
    try:
        # reads config from config.ini (root-folder)
        config = ConfigParser()

        # config file doesn't require ../../ as this function is being called
        # by files (bot_master, app, back_end) in the root folder
        config.read("config.ini")

        host = config["database"]["Host"]
        username = config["database"]["username"]
        password = config["database"]["password"]

        return (0, MySQLdb.connect(host=host, user=username, passwd=password, db="Bots_DB"))
    except MySQLdb.OperationalError as err:
        return (1, err)

if __name__ == '__main__':
    connect_to_db()
