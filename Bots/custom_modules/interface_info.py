#!/usr/bin/env python3
import subprocess
import os

def run_cmd(cmd):
    ''' returns command output '''
    res= subprocess.run(cmd.split(), stdout=subprocess.PIPE)
    return res.stdout.decode()

def get_packet_stats(intf):
    ''' returns packet stats for intf passed,
        uses /sys/class/net/intf/ dir to get
        packet stats for an interface '''
    base_dir = "/sys/class/net/" + intf
    stats = {"recv_packets": {"number": 0, "size": 0},
             "sent_packets": {"number": 0, "size": 0}}

    # checks if interface has packet stats available
    if not os.path.exists(base_dir): return stats

    stats_dir = base_dir + "/statistics"

    for i in ["sent", "recv"]:
        key = "sent_packets" if i == "sent" else "recv_packets"
        prepend = "tx_" if i == "sent" else "rx_"

        stat_file = prepend + "packets"
        cmd = "cat {}/{}".format(stats_dir, stat_file)
        stats[key]["number"] = int(run_cmd(cmd).replace("\n", ""))

        stat_file = prepend + "bytes"
        cmd = "cat {}/{}".format(stats_dir, stat_file)
        stats[key]["size"] = int(run_cmd(cmd).replace("\n", ""))

    return stats

def ip_parser(output, is_ip=False):
    ''' parses ip output - only works when specific info has been req, e.g.
        only ip (-0) or mac (-4) '''
    interfaces = {}

    start_of_interface = False
    interface_name = ""

    for line in output:
        # keep this line here to prevent index error on empty list items
        if not line: continue

        if start_of_interface:
            # gets intf info
            tmp = line.strip().split()

            if is_ip:
                # removes size of network / off ip addr
                interfaces[interface_name] = tmp[1].split("/")[0]
            else:
                interfaces[interface_name] = tmp[1]
            start_of_interface = False

        # check if start of line is number
        # indicates start of new interface info
        if line[0] in "0123456789":
            start_of_interface = True
            #[:-1] to get rid of colon at end of intf name
            interface_name = line.split()[1][:-1]

    return interfaces

def get_interface_info():
    ''' returns info of interfaces using output from commands:
        ip (ip and mac) and /sys/class/net/interface_name/'''
    # Mac addresses and IP commands
    mac_addrs = run_cmd("ip -0 addr show").split("\n")
    ip_addrs = run_cmd("ip -4 addr show").split("\n")

    mac = ip_parser(mac_addrs)
    ip = ip_parser(ip_addrs, is_ip=True)

    interfaces = {}

    # combines mac and ip and gets packet stats for intf
    for intf in ip:
        interfaces[intf] = {"ip": ip[intf]}

        if intf in mac.keys():
            interfaces[intf]["ether"] = mac[intf]

        packet_stats = get_packet_stats(intf)
        interfaces[intf]["sent_packets"] = packet_stats["sent_packets"]
        interfaces[intf]["recv_packets"] = packet_stats["recv_packets"]

    return interfaces

if __name__ == '__main__':
    x = interface_info()
    print(x)
