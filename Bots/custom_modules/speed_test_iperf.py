#!/usr/bin/env/python3
import subprocess
import time
import os

''' Uses IPERF3 to perform speedtest '''

servers = [("bouygues.iperf.fr", 5200), ("ping.online.net", 5200),
           ("ping-90ms.online.net", 5200), ("speedtest.serverius.net", 5002),
           ("iperf.eenet.ee", 5201), ("iperf.volia.net", 5201)]

''' runs iperf command and returns speed '''
def run_iperf(server, port, reverse=""):
    cmd = "iperf3 -c {} -p {} {}".format(server, port, reverse)
    res = subprocess.run(cmd.split(), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    out = (res.stdout if res.stdout else res.stderr).decode()
    print(out)
    #checks if iperf output is an error or not
    if out[:13] == "iperf3: error":
        return -1
    else:
        return out

''' interps output of iperf to get speed '''
def interp_iperf(out, opt=""):
    x = out.split("\n")

    #gets line that contains average speed for upload or download
    ind = -4 if opt == "-R" else -5
    tmp = x[ind].split()

    ind = tmp.index("Mbits/sec")
    return float(tmp[ind - 1])

''' tests network speed using iperf command and servers '''
def speed_test(opt=""):
    while True:
        for i in servers:
            server, port = i
            o = run_iperf(server, port, reverse=opt)

            if o != -1:
                speed = interp_iperf(o, opt=opt)
                return speed
        time.sleep(1)
