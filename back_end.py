import datetime

from Bots.custom_modules.colour_print import colourp
from Bots.custom_modules.database import connect_to_db


def get_master_resources():
    ''' returns resources of master bot '''
    cols = ["ram_perct", "cpu_perct"]
    res = DB_get("BotMaster", cols)[0]

    return {"ram_perct": res[0], "cpu_perct": res[1]}

def get_bot_status(ID):
    ''' returns the status of the bot and time since last connected '''
    cols = ["status", "connected_time"]
    res = DB_get("Bots", cols, filter=("MAC_Addr", '"%s"' % ID))[0]
    connected_time = res[1]

    # calcs time diff from connceted time to cur time
    cur_time = datetime.datetime.now()
    diff_secs = (cur_time - connected_time).total_seconds()
    timestamp = get_timestamp(diff_secs)

    return {"status": res[0], "time": timestamp}

def get_timestamp(secs):
    ''' returns timestamp from secs passed '''
    mins = round(secs / 60, 1)
    hours = round(secs / (60 * 60), 1)

    indicator = ""

    if hours >= 1: return "{}hrs".format(hours)
    elif mins >= 1: return "{}mins".format(mins)
    else: return "{}secs".format(round(secs, 1))

def get_bot_resources(ID):
    ''' gets bot resources
        :param ID: id of bot
        :output Dict: dict containing bot resources '''
    # get resources and status for bot from DB
    cols = ["OS_name", "cpu_model", "cpu_perct", "dl_speed", "ul_speed", "cpu_core_percts", "ram_total", "ram_used", "swap_total", "swap_used",
            "s_number", "s_size", "r_number", "r_size", "storage_devices"]

    b_res = DB_get("Bots", cols, filter=("MAC_Addr", '"%s"' % ID))[0]

    # convert to format (Dict) used by template
    bot = {"ID": ID}

    for i in range(len(cols)):
        bot[cols[i]] = b_res[i]

    # cpu cores
    tmp = ""
    cores = eval(bot["cpu_core_percts"])

    for i in range(len(cores)):
        tmp += "{} ({}%), ".format(i + 1, cores[i])

    bot["cpu_core_percts"] = tmp[:-2]

    # converts B to GB
    bytes = ["ram_used", "ram_total", "swap_used", "swap_total", "r_size", "s_size"]
    for i in bytes:
        bot[i] = round(bot[i] / 1000**3, 2)

    # gets perct used for ram and swap
    bot["ram_perct"] = round((bot["ram_used"] / bot["ram_total"]) * 100, 0)

    if bot["swap_total"] != 0:
        # precents divideByZero errors if no swap
        bot["swap_perct"] = round((bot["swap_used"] / bot["swap_total"]) * 100, 0)
    else:
        bot["swap_perct"] = 0

    # convert storage devices from str to dict
    bot["storage_devices"] = eval(bot["storage_devices"])

    # return template with resources
    return bot

''' checks if bot id exists in table '''
def valid_bot_id(ID):
    #checks bot id passed exists in table
    id = str(ID).replace('"', "").replace("'", "")
    tmp = DB_get("Bots", ["MAC_Addr"], filter=("MAC_Addr", '"%s"' % id))

    print(id)
    print(tmp)

    return 0 if not tmp else id

''' returns latest cmd id '''
def get_latest_cmd_id():
    #gets all cmd IDs
    ids = DB_get("Commands", ["ID"])

    #returns last id of last cmd added
    return int(ids[-1][0])

''' ====================================================================================
     Functions used by flask app to interact with DB
    ==================================================================================== '''

''' returns connection to DB '''
def db_connect():
    err, db = connect_to_db()

    if err:
        colourp("error", "unable to connect to DB", db)
    else:
        return db

''' returns output of running sql passed on database '''
def DB_exec(sql):
    db = db_connect()
    cursor = db.cursor()

    cursor.execute(sql)
    res = cursor.fetchall()

    db.commit()
    db.close()

    return res

''' inserts or modifys data into/in the DB
    if is_cmd is set to true it returns id of command uploaded '''
def DB_post(table, cols, data, is_cmd=0):
    db = db_connect()
    cursor = db.cursor()

    #convert cols to format used in sql
    c = "".join([i + ", " for i in cols])[:-2]
    d = []

    #converts data to format used in sql
    for i in data:
        if isinstance(i, str):
            d.append('"%s", ' % i)
        else:
            d.append('%s, ' % i)
    d = "".join(d)[:-2]

    sql = "INSERT INTO {} ({}) VALUES ({})".format(table, c, d)

    cursor.execute(sql)

    db.commit()
    db.close()

    #returns id of cmd uploaded or msg indicating upload worked
    if is_cmd:
        return get_latest_cmd_id()

    return "uploaded"

''' gets data from the DB
    :param table: table to get data from
    :param cols: columns to get
    :param filter: filter to use with WHERE clause

    :output dict: dict of table content '''
def DB_get(table, cols, filter=""):
    db = db_connect()
    cursor = db.cursor()

    #converts col names to format that can be used in select statement
    c = "".join([i + ", " for i in cols])[:-2]

    #checks if filter has to be applied and creates select statement
    if filter:
        sql = "SELECT {} FROM {} WHERE {} = {}".format(c, table, filter[0], filter[1])
    else:
        sql = "SELECT {} FROM {}".format(c, table)

    cursor.execute(sql)
    res = cursor.fetchall()
    db.close()

    return res

if __name__ == '__main__':
    x = get_bot_status("3c:95:09:9f:7e:b1")
