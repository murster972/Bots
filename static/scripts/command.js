/* uses ajax to send post request and upload command
   cmd format is [command, bots] */
function upload_command(cmd){
    $.ajax({
        type: 'post',
        url: '/cmd_post',
        data: JSON.stringify(cmd),
        contentType: "application/json; charset=utf-8",
        success: function(reply){
            console.log(reply)
            if(reply == "error"){
                //set error
            } else{
                //set succesful css
                toggle_popup("#27AE60", "Sent!");
            }
        },
        error: function(reply){
            // set error
        }
    });
}

function delete_command(cmdID){
    $.ajax({
        type: 'post',
        url: '/cmd_delete',
        data: JSON.stringify(cmdID),
        contentType: "application/json; charset=utf-8",
        success: function(reply){
            console.log(reply)
            if(reply == "error"){
                //set error
            } else{
                location.reload();
                //set succesful css
            }
        },
        error: function(reply){
            // set error
        }
    });
}

/* used to upload command on bot pages */
function bot_command(id){
    let cmd = $("#cmd_input input").val().trim();

    if(cmd.length == 0){
        toggle_popup("#D14C4C", "Invalid Bots or Command!");
    }
    else{
        upload_command([cmd, [id]]);
    }
}
