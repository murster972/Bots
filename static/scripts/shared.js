/* shared js code small enough not to have its own file */
$(document).ready(function(){
    $("#table .drop_butn").click(function(){
        // shows/hides details in cmd sections
        let details = $(this).next();
        $(details).toggleClass("hidden");

        // set drop_down arrow to active
        let dd_arrow = $(this).children()[0];
        $(dd_arrow).toggleClass("active");
    })

    $(".text_input_butn.clear").click(function(){
        // clear input button
        let input = $(this).next();
        $(input).val("");
    })
})

var popupAcitve = 0;

/* toggles popup for sending commands */
function toggle_popup(bg, text){
    popupAcitve = 1;
    $("#popup").addClass("active");
    $("#popup").css("background-color", bg);
    $("#popup").text(text);
    setTimeout(function(){
        $("#popup").removeClass("active");
        popupAcitve = 0;
    }, 1100)
}
