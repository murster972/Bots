/* js for drop-down menu */
$(document).ready(function(){
    $(".dropdown.container .radio_button span").click(function(){
        // NOTE: stores selected ids in container name attr
        // current drop-down container
        let dd_cont = $(this).parent().parent();

        //bot_id of clicked
        let bot_id = $(this).parent().prev().text();

        // checks if select all, select or unselect
        if($(this).hasClass("select_all")){
            let type = $(this).hasClass("selected");
            // gets all bot_ids for name and input text
            let name = "";
            let txt = "";
            let tmp = $(this).parent().nextAll();

            for(let i = 0; i < tmp.length; i++){
                let b = tmp[i];

                if(b.tagName == "P" && !type){
                    // select all
                    name += b.textContent + "_";
                    txt += b.textContent + ", ";
                } else if(b.className == "radio_button" && !type){
                    // select all
                    let child = $(b).children()[0];
                    $(this).addClass("selected");
                    $(child).addClass("selected");
                } else if(b.className == "radio_button" && type){
                    // unselect all
                    let child = $(b).children()[0];
                    $(this).removeClass("selected");
                    $(child).removeClass("selected");
                }
            }

            // set name and input text
            let input = $(dd_cont).prev();
            set_input_txt(input, txt);

            $(dd_cont).attr("name", name);

            return 0;

        } else if($(this).hasClass("selected")){
            $(this).removeClass("selected");

            // get selected bots from name and remove empty items
            let selected = get_bot_from_name(dd_cont);

            // remove bot from selected bots
            ind = selected.indexOf(bot_id);
            selected.splice(ind, 1);

            // convert bots back to format used for names
            let name = "";

            for(var i = 0; i < selected.length; i++){
                name += selected[i] + "_";
            }

            $(dd_cont).attr("name", name);

        } else{
            $(this).addClass("selected");

            // adds botid to container name
            let name = $(dd_cont).attr("name");
            $(dd_cont).attr("name", name + bot_id + "_");
        }

        // unselects select all
        tmp = $($(dd_cont).children()[1]).children()[0];
        $(tmp).removeClass("selected");

        // assign selected bots to input text
        let bots = $(dd_cont).attr("name");

        if(bots.length > 0){
            bots = bots.replace(/\_/g, ", ");
        }

        // assumes drop_down container is directly after input element
        let input = $(dd_cont).prev();
        set_input_txt(input, bots);
    })

    $(".text_input_butn.down").click(function(){
        let dd_cont = $($(this).next()).next();

        $(dd_cont).toggleClass("hidden");
    })
})

function set_input_txt(input, bots){
    // adds "..." if to many bots to fit in input
    if(bots.length > 38){
        $(input).attr("value", bots.substring(0, 38) + "...");
    } else{
        $(input).attr("value", bots);
    }
}

function get_bot_from_name(container){
    // get selected bots from name and remove empty items
    let selected = $(container).attr("name").split("_");

    let ind = selected.indexOf("");
    selected.splice(ind, 1);

    return selected;
}
