/* ajax used for getting resources - and calls function to update
   html items based on if its overview or not */
function bot_resources(isOverview){
    $.ajax({
        type: 'get',
        url: '/resource_bot_get?ID=' + botID,
        success: function(res){
            // console.log(res);
            update_bot_resources(res, isOverview);
        },
        error: function(reply){
            // set error
        }
    });
}

function master_resources(){
    $.ajax({
        type: 'get',
        url: '/resource_master_get',
        success: function(res){
            update_master_resources(res);
        },
        error: function(reply){
            // set error
        }
    });
}

/* updates master resources */
function update_master_resources(res){
    $("#cpu_perct").text(res["cpu_perct"] + "%");
    $("#ram_perct").text(res["ram_perct"] + "%");

    update_circle("cpu_before", res["cpu_perct"]);
    update_circle("ram_before", res["ram_perct"]);
}

/* updates bot resources with new values */
function update_bot_resources(res, isOverview){
    // updates html thats on both overview and resources page
    $("#bot_cpu_perct").text(res["cpu_perct"] + "%");

    update_cores(res["cpu_core_percts"]);

    let ram = res["ram_perct"]
    $("#bot_ram_perct span").text(ram + "%");
    update_circle("ram_before", ram);

    $("#dl_speed").text(res["dl_speed"] + "Mbps");
    $("#ul_speed").text(res["ul_speed"] + "Mbps");
    update_network_indicators(isOverview, res["dl_speed"], res["ul_speed"]);

    if(isOverview){
        // OS name
        $("#OS_name").text("OS: " + res["OS_name"]);
    } else {
        // ram
        $("#ram_detail #used").text(res["ram_used"]);
        $("#ram_detail #total").text(res["ram_total"]);

        // cpu cores
        $("#cores_perct").text(res["cpu_core_percts"])

        // swap
        $("#bot_swap_perct span").text(res["swap_perct"] + "%");
        $("#swap_detail #used").text(res["swap_used"]);
        $("#swap_detail #total").text(res["swap_total"]);
        update_circle("swap_before", res["swap_perct"]);

        // network - stats
        $("#in_pack_no").text(res["r_number"]);
        $("#ot_pack_no").text(res["s_number"]);
        $("#in_pack_size").text(res["r_size"]);
        $("#ot_pack_size").text(res["s_size"]);


        // network - graph
        graph.add_point("o", res["s_size"]);
        graph.add_point("i", res["r_size"]);
    }
}

/* updates network indidactors */
function update_network_indicators(isOverview, dl, ul){
    let max_speed = 500; //Mbps

    if(isOverview){
        let height = $("#network_speeds .bar").height();

        let dl_mapped = map(dl, 0, max_speed, 0, height);
        let ul_mapped = map(ul, 0, max_speed, 0, height);

        $(".bar.download .bar_before").height(dl_speed)
        $(".bar.upload .bar_before").height(ul_mapped)
    } else{
        // turn starts at -0.38 and ends at 0.38
        let speeds = [dl, ul];
        let names = ["download", "upload"];

        // maps dl, ul values to turn values and applies to approp indicator
        for(i = 0; i < speeds.length; i++){
            let s = speeds[i];
            let perct = s / max_speed * 100;
            let mapped_value = map(perct, 0, 100, -0.39, 0.39);

            let bar = $("#" + names[i] + "_speed .before");
            let bar_bg = (perct > 50) ? "#000" : "inherit";
            mapped_value = (perct > 50) ? -1.5 + mapped_value : mapped_value;

            bar.css("background-color", bar_bg);
            bar.css("transform", "rotate(" + mapped_value + "turn)");
        }
    }
}

/* updates cpu core circles */
function update_cores(c){
    let cores = c.split(", ");

    for(let i = 0; i < cores.length; i++){
        // how c arg is formatted - ["1 (12.0%)", "2 (19.4%)"]
        let perct = cores[i].split(" ")[1];
        perct = Number(perct.substring(1, perct.length - 2));
        update_circle("core_before_" + i.toString(), perct);
    }
}

/* turns circle to n pect  - assumes pect is passed as str */
function update_circle(id, perct){
    let circle = document.getElementById(id);
    let turn, colour;

    // apply colours - good (0-33.3), okay(33.3-66.6) and warning(66.6-100)
    if(Number(perct) < 33.3) colour = "#27AE60";
    else if(Number(perct) < 66.6) colour = "#FF6F4B";
    else colour = "#D14C4C";

    circle.style.background = colour;
    let parent = $("#" + id).parent();
    let bg_img = "linear-gradient(to right, transparent 50%, " + colour + " 0)";

    $(parent).css("background-image", bg_img);

    // "pie-chart" rotate
    // https://www.smashingmagazine.com/2015/07/designing-simple-pie-charts-with-css/
    if(Number(perct) < 50){
        // converts perctrees to turn value - p5.js function
        turn = map(perct, 0, 100, 0, 1).toString()

        /* see: http://ricostacruz.com/jquery.transit/ */
        circle.style.background = "";
    } else {
        turn = map(perct, 0, 100, 0, 1).toString() - 0.5;
    }

    circle.style.transform = "rotate(" + turn + "turn)";

    // sets text colour - gets parent (inner circle), then h2 that store %
    let nxt = $("#" + id).next()[0];
    let txt = $(nxt).children()[1];
    $(txt).css("color", colour);
}

/* creates circles used by core - c is list of core % usage */
function init_core_circles(c, p_id){
    let cores = c.split(", ");
    let len = cores.length;
    //let circle =

    let inner_circle_width = 130;
    let circle_width = 40;

    // width of first outer circle and init parent
    let width = inner_circle_width + (circle_width * len);
    let parent = $("#" + p_id);

    for(let i = len - 1; i >= 0; i--, width -= circle_width){
        // add new circle
        let s = i.toString();

        // adds "core" class to every circle bar first to ensure
        // abs postion and border is set
        if(i != len - 1) s += " core"

        let outer_circle = '<div class="outer_circle ' + s + '">\
                                <div class="before" id="core_before_' + i.toString() + '"></div>\
                            </div>';

        parent.append($(outer_circle));
        parent = $(".outer_circle." + i.toString());

        // set width/height of last circle added
        parent.width(width + "px");
        parent.height(width + "px");
    }

    // add inner circle
    let inner_circle = '<div class="inner_circle">\
                            <h1>CPU</h1>\
                            <h2 id="bot_cpu_perct"></h2>\
                        </div>';
    $(".outer_circle.0").append($(inner_circle));
}
