
/* updates bot status */
function bot_status(){
    $.ajax({
        type: 'get',
        url: '/get_bot_status?ID=' + botID,
        success: function(res){
            //console.log(res);
            update_bot_status(res["status"], res["time"]);
        },
        error: function(reply){
            // set error
        }
    });
}

function update_bot_status(stat, timestamp){
    console.log(stat)
    if(stat == 0){
        $("#bot_stat").text("Disconnected");
        $("#bot_stat").removeClass("good");
        $("#bot_stat").addClass("bad");
    } else{
        $("#bot_stat").text("Active - " + timestamp);
        $("#bot_stat").removeClass("bad");
        $("#bot_stat").addClass("good");
    }
}
