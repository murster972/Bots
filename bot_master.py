#!/usr/bin/env python3
import sys
import time
import select
import psutil
import socket
import MySQLdb
import datetime
from threading import Thread, Lock
from queue import Queue
from configparser import ConfigParser
from Bots.custom_modules.ip_port_val import validate_ip_port
from Bots.custom_modules.colour_print import colourp
from Bots.custom_modules.BOT_STATUS import BotStatus
from Bots.custom_modules.database import connect_to_db

class BotMaster:
    def __init__(self, ip, port):
        #ip and port input
        self.master_ip = ip
        self.master_port = port

        #verify ip and port
        validate_ip_port(self.master_ip, self.master_port)

        #DB upload queue
        self.upload_queue = {"master": {"cpu": 0, "ram": 0, "status": 1},
                             "bots": {}}

        #bots - format is ID: [sockt, status, missedCount, packetCount]
        self.bots = {}

        #checks if able to connect to DB
        self._connect_to_db()

        #lock used when accessing shared variables
        self.lock = Lock()

        #create socket and set optionsS
        try:
            self.master_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.master_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            # self.master_socket.setblocking(False)
            self.master_socket.bind((self.master_ip, int(self.master_port)))
            self.master_socket.listen(5)
        except PermissionError:
            colourp("error", "Unable to bind socket", "incorrect user permissions")
            sys.exit(-1)
        except (OSError, IOError) as err:
            colourp("error", "Creating socket caused the following error, please check that the IP and port are avaiable to use and on this machine", err)
            sys.exit(-1)

        #creates and runs threads
        resources = Thread(target=self.get_resources, args=[], daemon=True)
        db_upload = Thread(target=self._DB_uploader, args=[], daemon=True)
        send = Thread(target=self._send, args=[], daemon=True)
        recv = Thread(target=self._recv, args=[], daemon=True)

        resources.start()
        db_upload.start()
        send.start()
        recv.start()

        #listens for bots
        colourp("info", "BotMaster listening for bots at", "IP: {}, Port: {}".format(self.master_ip, self.master_port))

        bot_thread_count = 0
        max_thread_count = 10

        try:
            while True:
                c_sock, c_addr = self.master_socket.accept()

                if bot_thread_count >= max_thread_count:
                    #max number of bots already connected
                    colourp("error", "Max bots reached.")
                    c_sock.close()
                    continue
                else:
                    #start new bot thread
                    new_c = Thread(target=self._bot, args=[c_sock, c_addr], daemon=True)
                    new_c.start()

                    bot_thread_count += 1
        except KeyboardInterrupt:
            pass
        # except Exception as err:
        #     print("[ERROR] Following error occured when listening for bots: {}".format(err))
        finally:
            self._DB_uploader(closing=1)
            colourp("info", "Ending BotMaster")
            self.master_socket.close()

    ''' monitors and manages connection to bot '''
    def _bot(self, sock, addr):
        b_sock = sock
        ip, port = addr

        #get MAC addr and hostname
        self.lock.acquire()
        mac, name = eval(sock.recv(65).decode())
        self.lock.release()

        b_sock.setblocking(0)

        colourp("info", "bot connected", "Name - {}, IP - {}, port: {}, MAC: {}".format(name, addr[0], addr[1], mac))
        colourp("info", "Resource info will be periodically updated in database for bot", mac)

        #add new bot to db or updates if already in table
        db = self._connect_to_db()
        cursor = db.cursor()

        connected_time = datetime.datetime.now()

        #check if bot already in table
        cursor.execute('SELECT * FROM Bots WHERE MAC_Addr = "{}";'.format(mac))

        if cursor.fetchall():
            #Bot already exists in table - updates entry
            cursor.execute('UPDATE Bots SET name = "{}", IP_Addr = "{}", port_no = {}, status = 2, connected_time = "{}" where MAC_Addr = "{}";'.format(name, ip, port, connected_time, mac))
        else:
            #Bot doesn't exist in table - creatse new entry
            cursor.execute('INSERT INTO Bots (name, MAC_Addr, IP_Addr, port_no, status) values ("{}", "{}", "{}", "{}", 2);'.format(name, mac, ip, port))
        db.commit()

        #add to bot list and upload queue
        self.bots[mac] = [b_sock, "active", 0, 0]
        self.upload_queue["bots"][mac] = []

        #maintain client connection
        while True:
            packets_recv = self.bots[mac][3]
            time.sleep(30)

            try:
                b = self.bots[mac]
            except KeyError:
                # bot disconnected
                break

            continue
            #check if any packets recv since timer start
            if packets_recv == b[3] and b[2] <= self.MAX_MISSED_COUNT:
                #no packets recv and not maxMisedCount
                self.bots[mac][1] = "checking"
                self.bots[mac][2] += 1
            elif packets_recv == b[3] and b[2] >= self.MAX_MISSED_COUNT:
                #no packets recv and max missed count is exceeded
                #so assume client has timedout/disconnected and close
                #connection and thread
                del self.bots[mac]
                del self.upload_queue["bots"][mac]
                b_sock.close()
                cursor.close()
                db.close()
                break
            else:
                #packets recv - reset state and missedCount
                b[mac][1] = "active"
                b[mac][2] = 0

    ''' Adds status change of bot to DB upload queue '''
    def _update_bot_status(self, id, status):
        self.upload_queue["bots"][id].append(["Status", status])

    ''' Checks if Bot sockets have recv anything and uploads to DB '''
    def _recv(self):
        #get all client sockets
        while True:
            socks = [self.bots[i][0] for i in self.bots.keys()]

            r, w, e = select.select(socks, [], [], 0)

            #goes through each socket checking if any data recv
            for sock in r:
                d = sock.recv(65536).decode()

                # extracts all tuples from data. This includes if multiple
                # are sent by bot and recv at the same time, they wouldnt be
                # merged into one like they would if using "eval()"
                tuples = self.get_tuples(d)

                # ensure only blank msg is still processed
                if len(tuples) == 0: tuples.append("")

                # loops through tuples recv
                for data in tuples:
                    #if blank msg bot has disconnected for some reason
                    if not data:
                        bot_id = self._socket_to_mac(sock)

                        colourp("info", "Bot has disconnected", bot_id)
                        self._update_bot_status(bot_id, 0)
                        continue

                    #checks if cmd results or resources
                    if data[0] == "Commands":
                        cmd_id, bot_id, output, srt_time, run_time = data[1]
                        self.upload_queue["bots"][bot_id].append(["BotCommands", data[1]])

                        colourp("info", "recv cmd output from {} for cmd-ID {}".format(bot_id, cmd_id), output)

                    elif data[0] == "Resources":
                        bot_id = data[1][0]
                        self.upload_queue["bots"][bot_id].append(["Resources", data[1]])

                    elif data[0] == "Network Speed":
                        bot_id = data[1][0]
                        self.upload_queue["bots"][bot_id].append(["Network Speed", data[1]])

                    else:
                        colourp("error", "Unknown msg recv from bot", data)
            time.sleep(1)

    ''' checks DB for new commands and sends them to approp clients '''
    def _send(self):
        db = self._connect_to_db()
        cursor = db.cursor()
        while True:
            new_cmds = self._get_new_cmds(db)

            #loop through and send each new cmd to approp bot
            for i in new_cmds:
                bot_id, cmd_id, cmd = i

                #checks bot is still connected - this is incase status has been changed after the command
                #was got from DB
                if bot_id not in self.bots:
                    continue

                colourp("info", "sending cmd to {}".format(bot_id), cmd)
                self.bots[bot_id][0].send("('cmd', '{}', '{}')".format(cmd_id, cmd).encode())

                #update command sent value - only after sent to client
                cursor.execute("UPDATE Bot_Commands SET Sent = 1 WHERE cmdID = {}".format(cmd_id))
                db.commit()

            time.sleep(5)
        db.close()

    ''' gets new cmds from DB '''
    def _get_new_cmds(self, db):
        cursor = db.cursor()

        #(botID, cmdID, cmd)
        new_cmds = []

        #check DB for new cmds to send to active bots
        cursor.execute("""SELECT cmdID, botID FROM Bot_Commands
                          INNER JOIN Bots ON Bot_Commands.botID = Bots.MAC_Addr AND Bots.status = 2 AND Bot_Commands.Sent = 0;""")
        tmp = cursor.fetchall()

        #loop through new commands and get cmd to send
        for i in tmp:
            cmd_id, bot_id = i

            #get cmd to send
            cursor.execute('SELECT cmd FROM Commands WHERE ID={}'.format(cmd_id))
            cmd = cursor.fetchall()[0][0]

            new_cmds.append((bot_id, cmd_id, cmd))

        db.commit()
        cursor.close()

        return new_cmds

    ''' Updates BotMaster resource info and info from bots in queue
        to DB '''
    def _DB_uploader(self, closing=0):
        #creates cursor to use with DB connection
        db = self._connect_to_db()
        cursor = db.cursor()

        #upload master ip, port and OS
        cursor.execute('UPDATE BotMaster SET IP_Addr = "{}", port = {} where ID = 1;'.format(self.master_ip, self.master_port))

        while True:
            #update master resources
            ram = self.upload_queue["master"]["ram"]
            cpu = self.upload_queue["master"]["cpu"]
            cursor.execute('UPDATE BotMaster SET ram_perct = {}, cpu_perct = {} where ID = 1;'.format(ram, cpu))

            # List of bots to remove from upload_queue because they've disconnected or
            # timed-out
            removed_bots = []

            #check bot queues
            for c in self.bots.keys():
                to_upload = self.upload_queue["bots"][c]

                if not to_upload: continue

                #go through data to upload for bot
                for i in to_upload:
                    table = i[0]
                    data = i[1]

                    if table == "BotCommands":
                        # updates command with results from bot executed command
                        colourp("info", "Updating entry for cmd {} from bot {}".format(data[0], c))
                        sql = 'UPDATE Bot_Commands SET Output = "{}", srt_time = "{}", run_time = "{}" WHERE cmdID = "{}" AND botID = "{}";'.format(data[-3], data[-2], data[-1], data[0], c)
                        cursor.execute(sql)
                    elif table == "Resources":
                        # colourp("info", "Updating bot resource info for", c)
                        mac, os_name, cpu_model, cpu_perct, cpu_core_perct, ram_total, ram_used, swap_total, swap_used, s_number, s_size, r_number, r_size, storage_devices = data
                        sql = 'UPDATE Bots SET OS_name = "{}", cpu_model = "{}", cpu_perct = {}, cpu_core_percts = "{}", ram_total = "{}", ram_used = "{}", swap_total = "{}", swap_used = "{}", s_number = "{}", s_size = "{}", r_number = "{}", r_size = "{}", storage_devices = "{}" WHERE MAC_Addr = "{}";'.format(os_name, cpu_model, cpu_perct, cpu_core_perct, ram_total, ram_used, swap_total, swap_used, s_number, s_size, r_number, r_size, storage_devices, c)
                        cursor.execute(sql)
                    elif table == "Network Speed":
                        colourp("info", "Updating network speeds for", c)
                        mac, dl, ul = data
                        sql = 'UPDATE Bots SET dl_speed = {}, ul_speed = {} WHERE MAC_Addr = "{}"'.format(dl, ul, c)
                        cursor.execute(sql)
                    elif table == "Status":
                        status = data
                        colourp("info", "Changing status for bot {}".format(c), status)
                        sql = 'UPDATE Bots SET status = {} WHERE MAC_Addr = "{}"'.format(status, c)
                        cursor.execute(sql)
                        removed_bots.append(c)
                    else:
                        raise Exception("[ERROR] Only command results can be uploaded at the moment.")
                self.upload_queue["bots"][c] = []

            db.commit()

            # removes disconnected/timed out bots
            for bot_id in removed_bots:
                del self.bots[bot_id]

            if closing:
                break

            time.sleep(1)

    def _connect_to_db(self):
        error, db = connect_to_db()

        if error:
            colourp("error", "Unable to connect to DB", db)
            sys.exit(-1)
        else:
            return db

    ''' runs sql code on DB and returns results if any '''
    def _run_sql(self, cursor, sql):
        #runs code
        try:
            pass
        except:
            pass

    ''' Gets CPU and ram info for device running BotMaster '''
    def get_resources(self):
        while True:
            ram = psutil.virtual_memory().percent
            cpu = psutil.cpu_percent()

            self.upload_queue["master"]["ram"] = ram
            self.upload_queue["master"]["cpu"] = cpu

            time.sleep(15)

    ''' returns bots mac-address based on a socket passed '''
    def _socket_to_mac(self, socket):
        for mac in self.bots.keys():
            s = self.bots[mac][0]

            #checks if same socket
            if s == socket:
                return mac

    ''' extracts tuple/s from string. differs from "eval" as it allows can handle
        multiple tuples in single string. '''
    def get_tuples(self, data):
            #NOTE: doesnt account for triple quotes yet
            l = len(data)

            open = False
            inside_str = False
            str_type = ""
            b_count = 0
            open_ind = 0

            tuples = []

            for i in range(l):
                # looks for first (NOT inner) opening bracket and its closing to indentify tuples
                val = data[i]

                # checks to see if starting str or inside str if its ended
                # if starting sets true and type of quote used to start str
                if inside_str and val == str_type:
                    inside_str = False
                elif not inside_str and (val == "'" or val == '"'):
                    inside_str = True
                    str_type = val

                # looks for closing when in open
                if open:
                    if val == "(" and not inside_str:
                        b_count += 1
                    elif val == ")" and not inside_str:
                        b_count -= 1

                    # all brackets have been paired, including very outer so this ind is end of tuple
                    if b_count == 0:
                        tuples.append(eval(data[open_ind:i + 1]))
                        open = False

                # sets to open/resets count if open bracket and not inside str
                elif not open and not inside_str and val == "(":
                    open = True
                    b_count = 1
                    open_ind = i

            return tuples

def get_config_details():
    ''' values from config file '''
    config = ConfigParser()
    config.read("config.ini")

    return config

def start_bot_master():
    config_values = get_config_details()
    ip = config_values["bot-master"]["IP-Address"]
    port = config_values["bot-master"]["Port-Number"]

    BotMaster(ip, port)

if __name__ == '__main__':
    start_bot_master()
