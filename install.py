#!/usr/bin/env python3
from configparser import ConfigParser
import subprocess
import os

''' Installs/sets-up all packages, modules and config used by Bots '''
def run_cmd(cmd):
    ''' runs and returns output of cmd '''
    res = subprocess.run(cmd.split(), stdout=subprocess.PIPE)
    return res.stdout.decode()

def package_installed(package):
    ''' returns True if a package is installed, else False '''
    check = run_cmd("apt-cache policy {}".format(package)).split("\n")
    installed = True

    # looks for "installed" section of output and checks if not instaleld
    for line in check:
        if "Installed" in line and "(none)" in line:
            installed = False
            break

    return installed

def install_package(package):
    ''' installs package '''
    os.system("apt-get install " + package)

def pip3_installed(name):
    ''' checks if pip3 package is installed '''
    res = run_cmd("pip3 show " + name)

    # cmd output is empty if not installed
    return True if res else False

def pip3_install(name):
    ''' installls pip3 package '''
    os.system("pip3 install " + name)

def is_sudo():
    ''' checks if sudo or root '''
    if os.getuid() != 0:
        print("Please run this script as root or with sudo.")
        exit(-1)

def create_config():
    ''' creates config file - and returns config values '''
    print("The following value will be used to create the configuration file.")

    sections = [("bot-master", "Use 0.0.0.0 to have BotMaster listen on all interfaces."),
                ("database", "NOTE: Using root can lead to security issues."),
                ("flask-server", "")]
    section_values = [("IP-Address", "Port-Number"),
                      ("Host", "username", "password"),
                      ("IP-Address", "Port-Number")]

    config = ConfigParser()

    print("WARNING: using root user for database can be a security risk.")

    for i in range(len(sections)):
        section = sections[i][0]
        msg = sections[i][1]
        print("--==--== %s ==--==--" % section)

        if msg:
            print(msg)
            print("--==--==%s==--==--" % ("=" * (len(section) + 2)))

        config[section] = {}

        for v in section_values[i]:
            config[section][v] = input(v + ": ")

    print("Creating config file: config.ini")
    with open("config.ini", "w") as config_file:
        config.write(config_file)

    return config

def db_setup(config):
    ''' sets up DB '''
    print("Setting-up Database.")
    os.system("mysql_secure_installation")

    print("Creating Bots DB and tables")
    os.system("mysql -u root -p < Bots/Database/tables.sql")

    user = config["username"]
    pasw = config["password"]
    host = config["host"]

    if user.lower() != "root":
        os.system("sudo mysql --execute=\"CREATE USER '{}'@'{}' IDENTIFIED BY '{}'; GRANT ALL PRIVILEGES ON Bots_DB.* TO '{}'@'{}';\"".format(user, host, pasw, user, host))
        print("User created for use with Bots Database with values in config file")
    else:
        print("Root user exists by default")

def main():
    is_sudo()

    # welcome msg
    print("This program will install the nessecary packages and modules needed to run the bot program. it will also create the configuration file.")

    opt = input("Are you installing to use bot-master on this machine?[y/n]: ").lower()
    is_master = True if opt == "y" or opt == "yes" else False

    os.system("apt-get update")

    # installs packages needed for bot-master and bot
    print("Searching for packages/modules, and installing if needed.")

    # [(package,pip3 or apt-get install), ...]
    to_install = [("python3-pip", "apt"), ("speedtest-cli", "pip3"), ("net-tools", "apt"),
                  ("psutil", "pip3")]

    if is_master:
        to_install += [("mysql-server", "apt"), ("flask", "pip3"), ("python3-mysqldb", "apt"),
                       ("gunicorn", "pip3")]

    for i in to_install:
        package_name, install_type = i

        if install_type == "pip3":
            check = lambda n: pip3_installed(n)
            install = lambda n: pip3_install(n)
        else:
            check = lambda n: package_installed(n)
            install = lambda n: install_package(n)

        print("[SEARCHING] ", package_name)

        if check(package_name):
            print("[INSTALLED] %s is already installed" % package_name)
        else:
            print("[INSTALL] Attemping to install", package_name)
            install(package_name)

    # bot-master setup - packages, config file, modules and DB
    if is_master:
        db_config = create_config()["database"]
        db_setup(db_config)


    print("INSTALL COMPLETE - see readme.md for info on how to run Bots")

if __name__ == '__main__':
    main()
