#!/usr/bin/env python3
import re
import sys
import time
import select
import psutil
import socket
import platform
import subprocess
from random import randint
from queue import Queue
from threading import Thread, Lock
from Bots.custom_modules.ip_port_val import validate_ip_port
from Bots.custom_modules.speed_test import speed_test
# from Bots.custom_modules.ifconfig import parse_ifconfig
from Bots.custom_modules.interface_info import get_interface_info
from Bots.custom_modules.colour_print import colourp
from Bots.custom_modules.cpu_name import get_cpu_name
from Bots.custom_modules.storage_devices import get_storage_devices

class Bot:
    def __init__(self):
        #ip, port input and val
        self.ip = input("BotMaster IP: ")
        self.port = input("BotMaster Port: ")

        validate_ip_port(self.ip, self.port)

        #get source ip used by socket and the interface
        self.sock_ip = socket.gethostbyname(socket.getfqdn())
        self.if_used = self.get_interface()

        #mac address - used as bot ID
        self.mac = self.get_mac()

        #queue used to store cmds waiting to be ran
        self.cmds_queue = Queue()

        #queue used to store data to send to master
        #format: (table_name, (data))
        self.send_queue = Queue()

        #thread lock
        self.lock = Lock()

        try:
            #create socket and connect to BotMaster
            self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            self.sock.connect((self.ip, int(self.port)))
        except ConnectionRefusedError:
            colourp("error", "Connection to BotMaster refused", "({}:{})".format(self.ip, self.port))
            sys.exit(-1)
        except (OSError, IOError) as err:
            colourp("error", "Creating socket caused the following error", err)
            sys.exit(-1)

        colourp("info", "Connected to BotMaster at", "Name: IP: {} Port: {}".format(self.ip, self.port))
        colourp("info", "Bot will send peridoc updates of its resources to BotMaster")

        #send init info to BotMaster - mac and name
        to_send = str((self.mac, platform.node()))
        self.sock.send(to_send.encode())

        #create and start threads - send, command and resources
        send = Thread(target=self._send, args=[], daemon=True)
        cmds = Thread(target=self._commands, args=[], daemon=True)
        resc = Thread(target=self.get_resources, args=[], daemon=True)
        spds = Thread(target=self._network_speeds, args=[], daemon=True)

        send.start()
        cmds.start()
        resc.start()
        spds.start()

        #listen for msgs from botMaster
        colourp("info", "Listening for messages from BotMaster")
        try:
            while True:
                r, w, e = select.select([self.sock], [], [self.sock])

                for s in r:
                    self.lock.acquire()
                    try:
                        data = s.recv(2048).decode()
                    except ConnectionResetError:
                        raise KeyboardInterrupt()

                    self.lock.release()

                    if data: data = eval(data)

                    if not data:
                        #BotMaster program has crashed
                        colourp("error", "BotMaster unavaiable", "closing Bot")
                        self.sock.close()
                        colourp("info", "Bot socket closed.")
                        sys.exit(-1)
                    elif data[0] == "cmd":
                        cmd_id, cmd = data[1], data[2]
                        colourp("info", "command Recv", "ID - {}: {}".format(cmd_id, cmd))
                        self.cmds_queue.put((cmd_id, cmd))
                    else:
                        colourp("info", "No other data can be recieved")
                        continue

                time.sleep(5)
        except KeyboardInterrupt:
            self.sock.close()
            colourp("info", "Bot socket closed.")

    ''' checks queue for data and sends to botMaster '''
    def _send(self):
        while True:
            if not self.send_queue.empty(): self.lock.acquire()

            while not self.send_queue.empty():
                data = self.send_queue.get()
                data_type = data[0]

                if data_type != "Resources":
                    # prevents resources spamming terminal with messages
                    colourp("info", "sending data to BotMaster - {}".format(data_type))

                self.sock.send(str(data).encode())

            if self.lock.locked(): self.lock.release()

            time.sleep(1)

    ''' checks for and runs new commands sent by botMaster '''
    def _commands(self):
        while True:
            #get cmds untill queue is emptycmd_id
            while not self.cmds_queue.empty():
                #gets next cmd
                cmd_id, cmd  = self.cmds_queue.get()
                start = time.time()
                output = self._run_command(cmd)
                run = time.time() - start

                #adds results of ran cmd to queue to be master
                self.lock.acquire()
                self.send_queue.put(("Commands", (cmd_id, self.mac, output, start, run)))
                self.lock.release()
            time.sleep(5)

    ''' gets resources (ram, cpu, network) '''
    def get_resources(self):
        while True:
            #cpu, ram and swap
            #cpu_model = platform.processor()
            cpu_model = get_cpu_name()
            cpu_perct = psutil.cpu_percent()

            cpu_core_perct = str(psutil.cpu_percent(percpu=True))

            os_name = " - ".join(platform.linux_distribution())

            storage_devices = str(get_storage_devices())

            ram = psutil.virtual_memory()
            ram_total = ram.total
            ram_used = ram.used

            swap = psutil.swap_memory()
            swap_total = swap.total
            swap_used = swap.used

            #network info - sent (s) and recv (r)
            s, r = self.get_network_stats()
            s_number, s_size = s["number"], s["size"]
            r_number, r_size = r["number"], r["size"]

            self.lock.acquire()
            self.send_queue.put(("Resources", (self.mac, os_name, cpu_model, cpu_perct, cpu_core_perct, ram_total, ram_used, swap_total, swap_used, s_number, s_size, r_number, r_size, storage_devices)))
            self.lock.release()

            time.sleep(1)

    ''' gets network speeds and adds to send queue '''
    def _network_speeds(self):
        dl, ul = speed_test()

        self.lock.acquire()
        self.send_queue.put(("Network Speed", (self.mac, dl, ul)))
        self.lock.release()

        time.sleep(15)

    ''' runs command and returns output '''
    def _run_command(self, cmd):
        res = subprocess.run(cmd.split(), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        return (res.stdout if res.stdout else res.stderr).decode()

    ''' gets MAC address of current device '''
    def get_mac(self):
        #NOTE: will only currently work with linux
        ifconfig = get_interface_info()

        try:
            return ifconfig[self.if_used]["ether"].lower()
        except KeyError:
            #ifconfig returned no mac for interface, random mac
            #is generated and used
            return self._random_mac()

    ''' generates random mac address '''
    def _random_mac(self):
        #generates hex chars
        hc = [chr(c) for c in range(ord("a"), ord("f") + 1)]
        hc += [str(i) for i in range(10)]

        #picks random hex chars for mac
        mac = [hc[randint(0, 15)] + hc[randint(0, 15)] for i in range(6)]

        return ":".join(mac)

    ''' returns stats about packets sent/recv '''
    def get_network_stats(self):
        ifconfig = get_interface_info()[self.if_used]

        return (ifconfig["sent_packets"], ifconfig["recv_packets"])

    ''' sets name of interface used by src ip '''
    def get_interface(self):
        ifconfig = get_interface_info()

        #find interface that matches socket src ip
        for i in ifconfig.keys():
            intf = ifconfig[i]
            if "ip" not in intf: continue
            if intf["ip"] == self.sock_ip:
                return i


if __name__ == '__main__':
    Bot()
